// ------------------------------------------------------------
// Helpful? tips???
// ------------------------------------------------------------
extend class hdplayerpawn{
	double helptipalpha;
	string helptip;
	static void gametip(actor caller,string message){
		let hdp=hdplayerpawn(caller);
		if(hdp)hdp.usegametip(message);
		else caller.A_Log(message,true);
	}
	void usegametip(string arbitrarystring){
		arbitrarystring.replace("\r","");
		arbitrarystring.replace("\n\n\n","\n");
		arbitrarystring.replace("\n\n","\n");
		helptipalpha=1001.;
		helptip=arbitrarystring;
	}
	static void massgametip(string arbitrarystring){
		for(int i=0;i<MAXPLAYERS;i++){
			let hdp=hdplayerpawn(players[i].mo);
			if(hdp)hdp.usegametip(arbitrarystring);
		}
	}
	void showgametip(){
		if(
			!player
			||!hd_helptext
			||!hd_helptext.getbool()
		)return;
		helptip=StringTable.Localize("$TIP_TIP")..StringTable.Localize("$TIP_"..random[tiprand](0,31));
		helptipalpha=1001.;

		//log the tip in the console
		//delete carriage returns since they reset the font colour
		string helptiplogged=helptip;
		helptiplogged.replace("\n"," ");
		A_Log(helptiplogged,true);
	}
}
