// ------------------------------------------------------------
// Heat tracker
// ------------------------------------------------------------
class Heat:Inventory{
	double volume;
	double realamount;
	int burnoutthreshold;
	int burnouttimer;
	actor heatlight;
	BarrelExplodeMarker BarrelQueue; // [Ace] Not making a new thinker just for this. It's only used for barrels anyway.
	states{spawn:TNT1 A 0;stop;}
	default{
		+inventory.untossable //for some reason this works without it
		+inventory.keepdepleted
		+nointeraction
		inventory.amount 1;
		inventory.maxamount 9999999;
		obituary "%o was too hot to handle.";
	}
	static double GetAmount(actor heated){
		let htt=Heat(heated.findinventory("Heat"));
		if(!htt)return 0;
		return htt.realamount;
	}
	static Heat Inflict(
		actor heated,
		double addamount
	){
		//voodoo dolls were a mistake
		if(
			PlayerPawn(heated)
			&&!!heated.player
			&&!!heated.player.mo
		){
			heated=heated.player.mo;
		}

		let htt=Heat(heated.findinventory("Heat"));
		if(!htt){
			heated.A_GiveInventory("Heat",1);
			htt=Heat(heated.findinventory("Heat"));
			htt.amount=0;
		}
		htt.realamount+=addamount;
		return htt;
	}
	override void attachtoowner(actor user){
		super.attachtoowner(user);

		//get "absolute" height of the actor for measuring volume
		double uheight;
		if(
			!!user.player
			&&user.player.crouchfactor!=0
		)uheight/=user.player.crouchfactor;
		else if(
			HDMobBase(user)
		)uheight=HDMobBase(user).liveheight;
		else uheight=user.height;

		volume=(user.radius*user.radius*uheight);
		burnoutthreshold=max(40,((int(user.mass*(user.radius+uheight))+(user.spawnhealth()))>>5)+300);
		A_SetSize(user.radius,uheight);


		//why did Ace put this here and not in the barrel code
		if(user is "HDBarrel"){
			BarrelQueue = BarrelExplodeMarker.Get();
		}
		if(
			!BarrelQueue
			||BarrelQueue.LightCount < HDBarrel.MaxBarrelLights
		){
			heatlight=HDFireLight(spawn("HDFireLight",pos,ALLOW_REPLACE));
			heatlight.target=user;hdfirelight(heatlight).heattarget=self;
			if(BarrelQueue){
				BarrelQueue.LightCount++;
			}
		}
	}
	override void DoEffect(){
		if(!owner){destroy();return;}
		if(!owner.player&&isfrozen())return;

		//reset burnout if raised
		if(
			owner.bismonster
			&&!owner.bcorpse
			&&owner.health>=owner.spawnhealth()
		)burnouttimer=0;

		//make adjustments based on player status
		let hdp=hdplayerpawn(owner);
		if(hdp){
			if(
				hdp.health<1
				&&!!hdp.playercorpse
			){
				Heat.Inflict(hdp.playercorpse,realamount);
				destroy();
				return;
			}
		}

		//convert given to real
		if(amount){
			realamount+=amount;
			amount=0;
		}
		//clamp number to zero
		if(realamount<1){
			realamount=0;
			return;
		}
		int ticker=level.time;

		double burnthreshold=(frandom(100,140)+(owner.mass>>3));


		//flame
		if(
			!(ticker&(1|2))
			&&realamount>burnthreshold
			&&owner.bshootable
			&&!owner.bnodamage
			&&!owner.countinv("ImmunityToFire")
			&&burnoutthreshold>burnouttimer
		){
			if(owner.bshootable){
				realamount+=frandom(1.2,3.0);
			}
			if(owner.waterlevel<=random(0,1)){
				actor aaa;
				if(
					owner is "PersistentDamager"
					||realamount<600
					||burnouttimer>((burnoutthreshold*7)>>3)
				){
					burnouttimer++;
					aaa=spawn("HDFlameRed",owner.pos+(
						frandom(-radius,radius),
						frandom(-radius,radius),
						frandom(0.1,owner.height*0.6)
					),ALLOW_REPLACE);
					aaa.ReactionTime = BarrelQueue ? BarrelQueue.LightCount : 0;
				}else{
					burnouttimer+=2;
					aaa=spawn("HDFlameRedBig",owner.pos+(
						frandom(-radius,radius)*0.7,
						frandom(-radius,radius)*0.7,
						frandom(5,owner.height*0.7)
					),ALLOW_REPLACE);
					aaa.scale=(randompick(-1,1)*frandom(1.2,1.6),
						frandom(0.9,1.1))*clamp((realamount-200)*0.001,0.3,4.);
					aaa.ReactionTime = BarrelQueue ? BarrelQueue.LightCount : 0;

					if(heatlight){
						heatlight.args[0]=200;
						heatlight.args[1]=150;
						heatlight.args[2]=90;
						heatlight.args[3]=int(min(realamount*0.1,256));
					}
				}
				aaa.target=owner;
				aaa.A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP,volume:clamp(realamount*0.001,0,1.));
			}
		}

		//reset timer so charred remains can be reignited
		else if(
			burnouttimer>=burnoutthreshold
			&&realamount<100
		)burnouttimer=random((burnoutthreshold*3)>>2,burnoutthreshold);

		//damage
		if(
			!(ticker&(1|2))
			&&owner.bshootable
			&&!owner.bnodamage
			&&realamount>(burnthreshold*0.4)
		){
			double dmgamt=realamount*0.006;
			if(
				dmgamt<1.
				&&(frandom(0.,1.)<dmgamt)
			)dmgamt=1.;
			setxyz(owner.pos);
			owner.damagemobj(self,owner.player?owner.player.attacker:null,int(dmgamt),"hot",DMG_NO_ARMOR|DMG_THRUSTLESS);
			if(!owner){destroy();return;}
		}


		//conduct heat to nearby objects
		if(
			!(ticker&(1|2|4))
			&&realamount>0
		){
			double maxdist=realamount*0.1;  //max distance for fadeout
			blockthingsiterator itt=blockthingsiterator.create(owner,maxdist);
			while(itt.Next()){
				actor it=itt.thing;
				if(
					it!=owner
					&&it.bshootable
					&&owner.pos.z<it.pos.z+it.height
					&&!it.findinventory("ImmunityToFire")
				){
					let diff=realamount-GetAmount(it);
					if(diff<=0)continue;

					//check distance - dead centre should be 1.0
					double distt=it.distance2d(owner);
					if(distt>maxdist)continue;
					double proportion=1.-(distt/maxdist);

					double raddd=(radius+owner.radius)*0.8;
					double zzz=it.pos.z-owner.pos.z;

					//check if touching; target must be slightly higher if below
					proportion*=(
						distt<raddd
						&&zzz>-it.height*0.6
						&&zzz<owner.height
					)?0.02:0.005;

					double togive=min(diff*proportion,realamount);

//console.printf(owner.gettag().." with "..realamount.."  heated "..it.gettag().."   "..togive.."    at distance "..distt);

					if(togive>0){
						//prevent giving more heat than current amount
						if(togive>realamount)togive=realamount;

						Inflict(it,togive);
						realamount-=togive;
					}
				}
			}
		}



		//cooldown
		double reduce=max(realamount*0.003,1.);
		if(owner.vel dot owner.vel > 4)reduce*=1.6;

		if(owner.waterlevel>2)reduce*=10;
		else if(owner.waterlevel>1)reduce*=4;
		else if(owner.countinv("HDFireDouse"))reduce*=2;

		double aang=absangle(angle,owner.angle);
		if(aang>4.)reduce*=clamp(aang*0.4,1.,4.);
		if((!skill)&&owner.player)reduce*=2;
		realamount-=reduce;
		angle=owner.angle;

	}
}

class HDFireLight:PointLight{
	heat heattarget;
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=200;
		args[1]=150;
		args[2]=100;
		args[3]=0;
		args[4]=0;
	}
	override void tick(){
		if(!heattarget||!target){destroy();return;}
		if(isfrozen())return;
		setorigin(target.pos,true);
		if(args[3]<1){
			args[0]=0;
			args[1]=0;
			args[2]=0;
			args[3]=0;
		}
		else args[3]=int(frandom(0.9,0.99)*args[3]);
	}
}


